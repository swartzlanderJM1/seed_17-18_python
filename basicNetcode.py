#!/usr/bin/python

import sys
import demjson
import subprocess

base_url = "https://gcc-growth-chamber-c6c53.firebaseio.com/options/"

valOut = 0

if len(sys.argv) == 2:
    valOut = sys.argv[1]

print 'Will send this value:'
print valOut
print '\n'

# Get information from database
# Replace with "run" once updated
jsonVals = subprocess.check_output(["curl","-s","-k", base_url + "light" + "/target.json"])
print demjson.decode(jsonVals)

subprocess.call(["curl","-k", "-X", "PATCH", base_url + "light" +
                 ".json", "-d", "{\"actual\":" + str(valOut) + "}"])